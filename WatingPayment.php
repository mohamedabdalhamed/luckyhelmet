<?php 
ob_start();

    session_start();
include 'conect.php';

$stmt=$con->prepare("SELECT value FROM setting where id=1  ");
$stmt->execute();
$row=$stmt->fetch();
if($row['value']==1){

header("Location:undermaintenance.php");
}else{ 
    if(isset($_SESSION['email'])){
        $email=$_SESSION['email'];
        $stmt=$con->prepare("SELECT * FROM users where email=? AND status=1 ");
        $stmt->execute(array($email));
        if($stmt->rowCount() >0){
            session_unset();
            session_destroy();
            header('location:index.php');
            exit();    
        }
    }
      
if(isset($_SESSION['username'])){
        ?>


<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lucky Helmet (Store)</title>

    <link rel="shortcut icon" type="image/x-icon" href="img/logo.png">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kelly+Slab&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@300;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Aclonica&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Itim&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bree+Serif&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/fontawesome/css/all.min.css">

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


<script src="https://cube.paysky.io:6006/js/LightBox.js"></script>
</head>

<body id="bought">
    <!-- Modal -->
   


<?php 
 $id=$_SESSION['ID'];

   $stmt=$con->prepare("SELECT * FROM orders WHERE payment=0 AND user_id=?  ");
 $stmt->execute(array($id));
        $ite=$stmt->fetchAll();
        
foreach ($ite as $ke => $valu) {
echo '    <div class="modal" id="Modal'.$valu['id'].'" tabindex="-1" aria-labelledby="ModalLabel" aria-hidden="true">
';
?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        رقم الطلب
                        <span>#<?php echo $valu['id']; ?></span>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="date">
                        تاريخ الطلب
                        <span class="text-muted"><?php echo $valu['date_order']; ?></span>
                    </div>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">المنتج</th>
                                <th scope="col">الكمية</th>
                                <th scope="col">السعر</th>
                                <th scope="col">الخصم</th>
                                <th scope="col">الإجمالي</th>

                            </tr>
                        </thead>
                        <tbody>
                                     <?php
$id=$valu['id'];
   $stmt=$con->prepare("SELECT * FROM order_detilas WHERE order_id=?  " );
 $stmt->execute(array($id));
        $ite=$stmt->fetchAll();
        
                                 foreach ($ite as $key => $value) {
$id=$value['product_id'];
   $stmt=$con->prepare("SELECT * FROM items WHERE id=?");
 $stmt->execute(array($id));
        $ites=$stmt->fetch();
                                        
                                

                                ?>
                            <tr>
                                <th scope="row">

<?php 

echo $ites['name'];
?>
                                    
                                </th>
                                <td><?php 
echo $value['qty'];
                                ?>
                                </td>
                                <td><?php echo $ites['price'] ?> EGP</td>
                                <td><?php echo $valu['discount']; ?> EGP</td>

                                <td><?php 
echo $value['total'];
                                ?>EGP</td>

                            </tr>
                            <?php 
                                 
}
                                ?>
                     
                        </tbody>
                    </table>

                    <h3><?php echo $valu['total_after_count'] ?>: الاجمالي</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">إغلاق</button>
                </div>
            </div>
        </div>
    </div>
<?php 
}
?>
    <div class="page-container">
        <div class="not-footer">
            <div class="page-header">
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <div class="container-fluid">
                        <a class="navbar-brand logo" href="index.html">
                            <img src="img/logo.png" alt="Logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                            <ul class="navbar-nav mb-2 mb-md-0">

                                <li class="nav-item active">
                                    <a class="nav-link" aria-current="page" href="index.php">
                                        <i class="fas fa-home"></i>
                                        الرئيسية
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" aria-current="page" href="about.php">
                                        <i class="fas fa-question"></i>
                                        من نحن
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="license.php">
                                        <i class="fas fa-file-signature"></i>
                                        الشروط والأحكام
                                    </a>
                                </li>
                                       <?php 
                                 if(!isset($_SESSION['username'])){
                                ?> 
                                <li class="nav-item">
                                    <a class="nav-link" href="login.php">
                                        <i class="fas fa-user"></i>
                                        تسجيل الدخول - تسجيل
                                    </a>
                                </li>
                                <?php 
                                
                                 }
                                else{
                                    ?>
                                      <li class="btn-group nav-item">
                                    <button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        <i class="fas fa-user"></i>
 <?php
                                    echo $_SESSION['username'];
                                    ?>                                       </button>
                                    <ul class="dropdown-menu">

                                        <li><a class="dropdown-item" href="boughtproducts.php">
                                                <i class="fas fa-shopping-cart"></i>
                                                طلبات تم شراؤها
                                            </a></li>
                                            
                                        <li><a class="dropdown-item" href="wishlist.php">
                                                <i class="fas fa-heart"></i>
                                                المفضلة
                                            </a></li>

                                        <!-- <li>
                                            <a class="dropdown-item" href="waitingproducts.php">
                                                <i class="fas fa-cash-register"></i>
                                                طلبات بانظار الدفع
                                            </a>
                                        </li> -->
                                        <li>
                                            <hr class="dropdown-divider">
                                        </li>
                                        <li><a class="dropdown-item" href="usersettings.php">
                                                <i class="fas fa-user-cog"></i>
                                                إعدادات المستخدم
                                            </a></li>

                                        <li><a class="dropdown-item" href="logout.php">
                                                <i class="fas fa-sign-out-alt"></i>
                                                تسجيل الخروج
                                            </a></li>

                                    </ul>
                                </li>
                                    </a>
                                </li>
                                <?php
                                    
                                }
                                ?> 
                            </ul>
                        </div>
                        <a href="checkout.php" class="cart iconRound">
                            <span class="badge bg-secondary rounded-pill">          <span class="badge"></span></span>
                            <i class="fas fa-shopping-cart"></i>
                        </a>
                    </div>
                </nav>
            </div>
            <div class="page-body">

                <div class="container">
                    <div class="row">
                        <div class="container">
                        <div class="col-12 g-3 py-5"  style="display: flex;flex-direction: column-reverse;">
                            <?php
                                    $id=$_SESSION['ID'];

   $stmt=$con->prepare("SELECT * FROM orders WHERE payment=0 AND user_id=?");
 $stmt->execute(array($id));
        $ite=$stmt->fetchAll();
        if($stmt->rowCount() >0){
foreach ($ite as $ke => $valu) {

                             ?>
                            <div class="card card-min">
                                <div class="card-header">
                                    <h5>
                                        رقم الطلب
                                        <span>#<?php echo $valu['id'];?></span>
                                    </h5>
                                </div>
                                <div class="card-body">

                                 
                                    <?php 
                              $order_id=$valu['id'];
       
        $stmt=$con->prepare("SELECT
        order_detilas.*,
        items.*
    FROM 
    order_detilas
    INNER JOIN
    items
    ON
    items.id=order_detilas.product_id
    WHERE
      order_id =?
       ");

        $stmt->execute(array($order_id));
        $items=$stmt->fetchAll();
    
foreach ($items as $key => $value) {



   echo '<div class="offer">
                              
   <div class="card-title">';
   
                                      echo '<img src="Admin/upload/av/'.$value['image'].'" alt="" class="card-img-right">';
            
                                           echo '<h5>
                                                            '.$value['name'].'                           </h5>
                                           '; 
                                     
echo'
                                        </div>
                                        ';

                                      
                             echo '</div>';
    echo '<hr class="offer-split">';
                                
}
  
                                    ?>
                                

                                    <div class="btn-group">
                                    <div id="Error" class="alert alert-danger" style="display: none">
            must enter
            <strong>Merchant Id</strong> and
            <strong>Terminal Id</strong>
          </div>
                                        <?php 
                                         $order_id= $valu['id'];
                                         $paymentseleary=$valu['total_after_count']*100;
                                         echo '   <button type="button" class="btn btn-primary"     onclick="callLightbox('.$paymentseleary.','.$order_id.')">
                                         تدفع
                                                                         </button>
                                                                                        ';
                                     echo   '<button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                                                 data-bs-target="#Modal'. $valu['id'].'">
                                                               <i class="fas fa-info"></i>
                                                                             </button>';
                                        ?>
                                       
                                 
                                    </div>

                                </div>
                                <div class="card-footer text-muted">
<?php 
echo $valu['date_order'];
?>                                </div>
                            </div>
                            <?php 
}
}
else{

?>

                    <div  style="display: block;flex-direction: none">
                        <div class="emptyicon text-muted">
                            <i class="fas fa-store"></i>
                            <h2>لم تشتري منتجات بعد</h2>
                            <p>يمكنك الذهاب إلى الصفحة الرئيسية وشراء منتجات</p>
                        </div>
                        <a href="index.html" class="w-100 mt-4 btn btn-primary btn-lg confrimpayment">
                            <i class="fas fa-home"></i>
                            الذهاب إلى الصفحة الرئيسية
                        </a>
                    </div>
                </div>
                </div>
            </div>
        </div>
<?php

}
                            ?>
                        </div>

                
                    </div>
                </div>

            </div>

        </div>

        <footer class="page-footer my-5 text-muted text-center text-small">
            <div class="py-4">

                <ul class="list-inline">
                    <li class="list-inline-item"><a href="about.html">من نحن</a></li>
                    <li class="list-inline-item"><a href="license.html">شروط</a></li>
                    <li class="list-inline-item"><a href="dmca.html">DMCA</a></li>
                </ul>
                <ul class="social0media">

                    <li class="list-inline-item"><a href="https://www.facebook.com/LuckyHelmet0/" target="blank_">
                            <i class="fab fa-facebook-square"></i>
                        </a>
                    </li>

                    <li class="list-inline-item"><a href="https://www.instagram.com/lucky_helmet_st/" target="blank_">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>

                    <li class="list-inline-item"><a href="https://discord.gg/hnRvd5qVxp" target="blank_">
                            <i class="fab fa-discord"></i> </a>
                    </li>

                    <li class="list-inline-item"><a href="https://t.me/luckyhelmet" target="blank_">
                            <i class="fab fa-telegram-plane"></i> </a>
                    </li>

                </ul>

            </div>
            <ul class="list-inline payments">
                <li class="list-inline-item">
                    <img src="img/footer/visa_footer.png" alt="visa">
                </li>
                <li class="list-inline-item">
                    <img src="img/footer/vodafonecash.png" alt="vodafonecash">
                </li>
                <li class="list-inline-item">
                    <img src="img/footer/fawry-pay-english-logo-1.png" alt="fawry-pay">
                </li>
                <p class="my-1 mt-3 lucky0helmet">&copy; lucky helmet 2021</p>
            </ul>
        </footer>


    </div>




    <!-- Jquery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Swiperjs -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- My own script -->
    <script src="js/script.js"></script>
       <script src="js/cart.js"></script>
       
       <script type="text/javascript">

function callLightbox($price,$order_id) {
var order=$order_id;
  var amount =$price;
  var mID = "10205796375";
  var tID = "73119162";
  var mRef = "Txn-1234";
  
  if (mID === '' || tID === '') {
    document.getElementById("Error").style.display = "block";
    return;
  }

  document.getElementById("Error").style.display = "none";
  Lightbox.Checkout.configure = {
    MID: mID,
    TID: tID,
    AmountTrxn: amount,
    MerchantReference: mRef,
    TrxDateTime: '202009171418',
    SecureHash: '66316532643939342D383662392D343331332D383535382D386635636533336662303133',
    completeCallback: function (data) {
      console.log('completed');

      console.log(data);
    $.ajax({    
				url:"UpdateOrder.php",
				method:"POST",
                data:{order:order},
				success:function(data)
				{
             
                    console.log(data);
   }
			});
        
    },
    errorCallback: function (data) {
      console.log('error');
      console.log(data);
    },
    cancelCallback:function () {
      console.log('cancel');
    }
  };

  Lightbox.Checkout.showLightbox();
}
</script>

</body>

</html>
<?php 

}else
{
header('Location:login.php');

}

}
?>
