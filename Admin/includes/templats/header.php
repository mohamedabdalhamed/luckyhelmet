<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title><?php getTitle();?></title>
    <link rel="stylesheet" href="<?php echo $css;  ?>bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $css;  ?>font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $css;  ?>backend.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@600&display=swap" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <style>
    	.cairo{
    		font-family: 'Cairo', sans-serif;
    	}

    </style>
  </head>
  <body>  
   