  <?php 
    session_start();

    include 'init.php';
  $pagetitle="Categorise";
 
          $do=isset($_GET['do'])? $_GET['do']:'Manage'; //check if do==what ?  ****************************
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($do=='Manage'){


            
        $stmt=$con->prepare("SELECT
        categories.*,
        main_categories.name AS category_name
    FROM 
    categories  
    INNER JOIN
    main_categories
    ON
    main_categories.id=categories.main_Categories_id
       ");




        $stmt->execute();
        $row=$stmt->fetchAll();

         ?>
            <h1 class="text-center">Manage Sub Categories </h1>
            <div class="container">
                <a href="cate.php?do=Add" class=" addbtn btn btn-primary"><i class="fa fa-plus"> </i> Add New Category</a>

              
              <div class="table-responsive">
                  <table class=" min-table text-center table table-bordered ">
                     <tr>
                        <td>#ID</td>
                        <td>Sub_category Name</td>
                        <td>Category </td>
                        <td>Control</td>
                     </tr>




<?php   
foreach ($row as $k) {
  echo '<tr>';
      echo '<td>'.$k['id'].'</td>';
      echo '<td>'.$k['name'].'</td>';
      echo '<td>'. $k['category_name'].'</td>';      
      echo    "<th><a href='cate.php?do=Edit&catid=".$k['id'] ."' class='btn btn-xs btn-primary'>
              <i class='fa fa-edit'></i>Edit  </a><a href='cate.php?do=Delete&catid=".$k['id'] ."'  class='  comfirm  btn btn-xs btn-danger'>
              <i class='fa fa-close'></i>Delete  </a></th>";


echo '</td>';

  echo '</tr>';
}

?>

                  </table>

              </div>


           </div>
<?php
}


elseif($do=='Add'){

?>


            <h1 class="text-center">Add Categories </h1>
            <div class="container">
                <form class="form-horizontal"  action="?do=Insert" method="POST">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Name</label>
                       <div class="col-sm-10  col-md-6">
                           <input 
                                  type="text" 
                                  name="namec" 
                                  class="form-control" 
                                  autocomplete="off" 
                                  placeholder="Name Of Categorie" 
                                  required="required"  />

                       </div>
                       
                    </div>
                    
                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">select main catgories</label>
                       <div class="col-sm-10  col-md-6">
                           <select name="sub"  class="form-control" >
                           <?php 


     $stmt=$con->prepare("SELECT * FROM  main_categories");
     $stmt->execute();
     $cat=$stmt->fetchAll();        
               foreach ($cat as $ca) {
echo '<option value="'.$ca["id"].'">'.$ca["name"].'</option>';
               }
?>
                           </select>
                     

                       </div>
                       
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2  col-sm-10">
                            <input type="submit" value="Add Categories " class="btn btn-primary  btn-lg" >
                        </div>
                    </div>
                </form>
</div>

<?php
}

elseif($do == 'Insert'){
      if($_SERVER['REQUEST_METHOD']=='POST'){

           echo '<div class="container">';
           echo '<h1 class="text-center"> Insert Page</h1>';

           $cname=$_POST['namec'];
                   $main_categories=$_POST['sub'];




         $check_Cat=checkItem('name','categories',$cname);
        if($check_Cat==1){ 
                 $MSA='<div class="alert alert-danger">  This Categorie is Exist befor</div>';
                 Redurict($MSA,'');
       }else{

$stmt=$con->prepare("INSERT INTO categories(name,main_Categories_id)
                           VALUES( :cname,:mcategories)
	                            ");
$stmt->execute(array(
                    'cname'=>$cname,
                    'mcategories'=>$main_categories
	));
    $mas='<div  class="alert alert-success"> '.$stmt->rowCount().' Inserted'.'</div>';
    Redurict($mas,'back');          
}

      }else{

        echo '<div class="container">';
        $mas='<div  class="alert alert-danger">You Not Allow To Come Here </div> ';
        Redurict($mas,'jjj');


      }


	echo '</div>';

}

/////////////////////////////////////////////////////////////////////////////////////////////////

elseif($do=='Edit'){ 
 $catid=isset($_GET['catid']) && is_numeric($_GET['catid'])? intval($_GET['catid']):0;
           $stmt=$con->prepare("SELECT * 
                                FROM 
                                categories
                                WHERE 
                                     id=? 
                                   ");
           $stmt->execute(array($catid));
           $row=$stmt->fetch();

           $count=$stmt->rowCount();

            if($stmt->rowCount() > 0){  ?> 

<h1 class="text-center">Edit Categories </h1>
            <div class="container">
             <form class="form-horizontal"  action="?do=Update" method="POST">

                   <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">Name</label>
                    <div class="col-sm-10  col-md-6">
                  <input type="hidden" name="catid" value="<?php echo $row['id']; ?>" />

                     <input 
                     type="text" 
                     name="namec" 
                    class="form-control" 
                     placeholder="Name Of Categorie" 
                      required="required" 
                      value='<?php echo $row['name']; ?>'
                       />
                     </div>
                   </div>      <div class="form-group form-group-lg">
                     <label class="col-sm-2  control-label">select main catgories</label>
                       <div class="col-sm-10  col-md-6">
                           <select name="sub"  class="form-control" >
                           <?php 


     $stmt=$con->prepare("SELECT * FROM  main_categories");
     $stmt->execute();
     $cat=$stmt->fetchAll();        
               foreach ($cat as $ca) {
echo '<option value="'.$ca["id"].'">'.$ca["name"].'</option>';
               }
?>
                           </select>
                     

                       </div>
                       
                    </div>
                    





                    <div class="form-group">
                    <div class="col-sm-offset-2  col-sm-10">
                      <input type="submit" value="Save" class="btn btn-primary  btn-lg" >
                  </div>
                 </div>

             </form>
          </div>            
 <?php 
   /* if of end of edit page */
             }else{
              echo '<div class="container">';
              $mes='<div class="alert alert-danger">You are not alwoed to come here </div>';
              Redurict($mes);
              echo '</div>';

            }

}
elseif($do=='Update'){

  echo "<div class='container'>";
  echo '<h1 class="text-center">Update Category</h1>';


   if($_SERVER['REQUEST_METHOD']=='POST'){
      $catid         =$_POST['catid'];
      $catname       =$_POST['namec'];
       $main_categories=$_POST['sub'];
           
     
      $s=$con->prepare("UPDATE 
                             categories 
                           SET
                             name=? ,main_Categories_id=?
                           WHERE 
                              id=? 
                                     ");

      $s->execute(array( $catname,$main_categories,$catid));

    $mas='<div class="alert alert-success">'.$s->rowCount() .' Record Updated </div>';
           Redurict($mas,'back');


    


   }else{


              echo '<div class="container">';
              $mas='<div class=" alert alert-danger"> YOU Not alowed to came here </div>';
              Redurict($mas,'back');
              echo '</div>';

   }


echo "</div>";
}
////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
//

elseif($do=='Delete'){ 

echo '<div class="container">';
echo '<h1 class="text-center"> Delete page </h1>';

$catid=isset($_GET['catid']) && is_numeric($_GET['catid'])? intval($_GET['catid']):0;

    $checkItem=checkItem('id','categories',$catid);

          if($checkItem > 0){  
                  $stmt=$con->prepare("DELETE FROM categories WHERE id=?");
                  $stmt->execute(array($catid));
                  if($stmt->rowCount() > 0){
ob_start();

                   $mas='<div class="alert alert-success">'.$stmt->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';

}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
        include 'footer.php';

       ob_end_flush();

 //end of if check if is set session username camed or not****************************///////////////////////
   