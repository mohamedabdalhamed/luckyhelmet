$(document).ready(function(){


	load_cart_data();
	load_wish_data();
    
	function load_cart_data()
	{
		$.ajax({
			url:"fetch_cart.php",
			method:"POST",
			dataType:"json",
			success:function(data)
			{
				$('#cart_details').html(data.cart_details);
				$('.total_price').text(data.total_price);
				$('.badge').text(data.total_item);
			}
		});
	}
	function load_wish_data()
	{
		$.ajax({
			url:"fetch_wish.php",
			method:"POST",
			dataType:"json",
			success:function(data)
			{
								$('#whis_list').html(data.whis_details);

	
			}
		});
	}

	$('#cart-popover').popover({
		html : true,
        container: 'body',
        content:function(){
        	return $('#popover_content_wrapper').html();
        }
	});

	$(document).on('click', '.add_to_cart', function(){
		var product_id = $(this).attr("id");
		var product_name = $('#name'+product_id+'').val();
		var product_price = $('#price'+product_id+'').val();
		var product_quantity = $('#quantity'+product_id).val();
		var image= $('#image'+product_id).val();
		var action = "add";
		if(product_quantity > 0)
		{
			$.ajax({
				url:"action.php",
				method:"POST",
				data:{product_id:product_id, product_name:product_name, product_price:product_price, product_quantity:product_quantity,image:image, action:action},
				success:function(data)
				{
					load_cart_data();
                      var popup = document.getElementById("myPopup");

  popup.classList.toggle("shows");
				}
			});
		}
		else
		{
			alert("lease Enter Number of Quantity");
		}
	});

	$(document).on('click', '.ssss', function(){
		var wish_id = $(this).attr("id");
		var namew = $('#namew'+wish_id).val();
		var pricew = $('#pricew'+wish_id+'').val();
		var quantityw = $('#quantityw'+wish_id).val();
		var imagew= $('#imagew'+wish_id).val();

			$.ajax({
			url:"fetch_wish.php",
			method:"POST",
			dataType:"json",
			success:function(data)
			{
if(data.wish_id==wish_id){
		
	are(wish_id);
		}
		else if(data.wish_id=!wish_id){
				addtoFav(wish_id,namew,pricew,quantityw,imagew);
		}else{
				addtoFav(wish_id,namew,pricew,quantityw,imagew);
		}
	
			}
		

	

	}
	);
	});
	
	function addtoFav(wish_id,namew,pricew,quantityw,imagew){
	$.ajax({
				url:"wish_session.php",
				method:"GET",
				data:{wish_id:wish_id,namew:namew, pricew:pricew, quantityw:quantityw,imagew:imagew},
				success:function(data)
				{
			
				
				$("#"+wish_id+" .fa-heart").addClass('clicked');
				$("#"+wish_id+" .fa-heart").removeClass('ssss');

				}
			});

	}
function are(wish_id){

		var actions = 'remove';

			$.ajax({
				url:"wish_session.php",
				method:"GET",
				data:{wish_id:wish_id, actions:actions},
				success:function()
				{
					load_cart_data();
                    
                    				$("#"+wish_id+" .fa-heart").removeClass('clicked');
                    				$("#"+wish_id+" .fa-heart").addClass('ssss');


                    				                }
			});
}
	$(document).on('click','.delete_W', function(){
	
		var wish_id = $(this).attr("id");
		var actions = 'remove';

			$.ajax({
				url:"wish_session.php",
				method:"GET",
				data:{wish_id:wish_id, actions:actions},
				success:function()
				{
	load_wish_data();

					
                }
			});
	});

	$(document).on('click', '.delete', function(){
		var product_id = $(this).attr("id");
		var action = 'remove';
		if(confirm("Are you sure you want to remove this product?"))
		{
			$.ajax({
				url:"action.php",
				method:"POST",
				data:{product_id:product_id, action:action},
				success:function()
				{
					load_cart_data();
	$('#cart-popover').popover('hide');
 				$('#removeitems').text("Done Remove ");

                    
                    
                }
			})
		}
		else
		{
			return false;
		}
	});

	$(document).on('click', '#clear_cart', function(){
		var action = 'empty';
		$.ajax({
			url:"action.php",
			method:"POST",
			data:{action:action},
			success:function()
			{
				load_cart_data();
				$('#cart-popover').popover('hide');
 				$('#removeitems').text("Done Remove All ");
			}
		});
	});
    
});


    	$(document).on('click','#discount', function(){
		var name = $('#compan').val();
        		var start = $('#time').val();

			$.ajax({
				url:"discount.php",
				method:"POST",
				data:{name:name,start:start},
				success:function(data)
				{
                    $("#a").text(data);
					
					alert("تم اضافة الكوبون ");
					$.ajax({
						url:"Dis.php",
						method:"POST",
						data:{name:name,start:start},
						success:function(data)
						{
							$("#AFTER").html(data);
			
						}
					});

				}
			});
		
		return false;
	});
	