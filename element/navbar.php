     <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <div class="container-fluid">
                        <a class="navbar-brand logo" href="index.html">
                            <img src="img/Logo.png" alt="Logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                            <ul class="navbar-nav mb-2 mb-md-0">

                                <li class="nav-item active">
                                    <a class="nav-link" aria-current="page" href="index.php">
                                        <i class="fas fa-home"></i>
                                        الرئيسية
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" aria-current="page" href="about.php">
                                        <i class="fas fa-question"></i>
                                        من نحن
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="license.php">
                                        <i class="fas fa-file-signature"></i>
                                        الشروط والأحكام
                                    </a>
                                </li>

                           <?php 
                                 if(!isset($_SESSION['username'])){
                                ?> 
                                <li class="nav-item">
                                    <a class="nav-link" href="login.php">
                                        <i class="fas fa-user"></i>
                                        تسجيل الدخول - تسجيل
                                    </a>
                                </li>
                                <?php 
                                
                                 }
                                else{
                                    ?>
                                  <li class="nav-item">
                                    <a class="nav-link" href="logout.php">
                                        <i class="fas fa-file-signature"></i>
تسجيل الخروج                                    </a>
                                </li>
                                      <li class="nav-item">
                                    <a class="nav-link" href="">
                                        <i class="fas fa-user"></i>
    <?php
                                    echo $_SESSION['username'];
                                    ?>                                    </a>
                                </li>
                                <?php
                                    
                                }
                                ?> 
                            </ul>
                        </div>
                        <a href="checkout.php" class="cart iconRound">

                            <span class="badge bg-secondary rounded-pill">									<span class="badge"></span>
</span>
                            <i class="fas fa-shopping-cart"></i>
                        </a>
           
         </div>
        
</nav>

