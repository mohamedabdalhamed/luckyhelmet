<?php 
ob_start();

    session_start();
    include 'conect.php';

    $stmt=$con->prepare("SELECT value FROM setting where id=1  ");
    $stmt->execute();
    $row=$stmt->fetch();
    if($row['value']==1){
    
    header("Location:undermaintenance.php");
    }else{ 
        if(isset($_SESSION['email'])){
            $email=$_SESSION['email'];
            $stmt=$con->prepare("SELECT * FROM users where email=? AND status=1 ");
            $stmt->execute(array($email));
            if($stmt->rowCount() >0){
                session_unset();
                session_destroy();
                header('location:index.php');
                exit();    
            }
        }
  
        
    if(isset($_SESSION['username'])){//start if of is set session or no?///////////////////////////////////////
    	header('location:index.php');
    }//end if of is set session or no?/////////////////////////////////////////////////////////////////////////


?>

<html lang="en">

<head>
        <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Lucky Helmet (Store)</title>

    <link rel="icon" type="image/png" href="img/logo.png">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kelly+Slab&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@300;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Aclonica&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Itim&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

    <link rel="stylesheet" href="css/fontawesome/css/all.min.css">

    <!-- <link href="css/cheatsheet.rtl.css" rel="stylesheet"> -->

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <link rel="stylesheet" href="css/style.css">
</head>

<body id="login">

    <div class="page-container">
        <div class="not-footer">
            <div class="page-header">
               
         <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <div class="container-fluid">
                        <a class="navbar-brand logo" href="index.html">
                            <img src="img/logo.png" alt="Logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                            <ul class="navbar-nav mb-2 mb-md-0">

                                <li class="nav-item active">
                                    <a class="nav-link" aria-current="page" href="index.php">
                                        <i class="fas fa-home"></i>
                                        الرئيسية
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" aria-current="page" href="about.php">
                                        <i class="fas fa-question"></i>
                                        من نحن
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="license.php">
                                        <i class="fas fa-file-signature"></i>
                                        الشروط والأحكام
                                    </a>
                                </li>

                           <?php 
                                 if(!isset($_SESSION['username'])){
                                ?> 
                                <li class="nav-item">
                                    <a class="nav-link" href="login.php">
                                        <i class="fas fa-user"></i>
                                        تسجيل الدخول - تسجيل
                                    </a>
                                </li>
                                <?php 
                                
                                 }
                                else{
                                    ?>
                                      <li class="btn-group nav-item">
                                    <button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        <i class="fas fa-user"></i>
 <?php
                                    echo $_SESSION['username'];
                                    ?>                                       </button>
                                    <ul class="dropdown-menu">

                                        <li><a class="dropdown-item" href="boughtproducts.php">
                                                <i class="fas fa-shopping-cart"></i>
                                                طلبات تم شراؤها
                                            </a></li>
                                            
                                        <li><a class="dropdown-item" href="wishlist.php">
                                                <i class="fas fa-heart"></i>
                                                المفضلة
                                            </a></li>

                                        <!-- <li>
                                            <a class="dropdown-item" href="waitingproducts.php">
                                                <i class="fas fa-cash-register"></i>
                                                طلبات بانظار الدفع
                                            </a>
                                        </li> -->
                                        <li>
                                            <hr class="dropdown-divider">
                                        </li>
                                        <li><a class="dropdown-item" href="usersettings.php">
                                                <i class="fas fa-user-cog"></i>
                                                إعدادات المستخدم
                                            </a></li>

                                        <li><a class="dropdown-item" href="logout.php">
                                                <i class="fas fa-sign-out-alt"></i>
                                                تسجيل الخروج
                                            </a></li>

                                    </ul>
                                </li>
                                    </a>
                                </li>
                                <?php
                                    
                                }
                                ?> 
                            </ul>
                        </div>
                        <a href="checkout.php" class="cart iconRound">

                            <span class="badge bg-secondary rounded-pill">		
                                							<span class="badge"></span>
</span>
                            <i class="fas fa-shopping-cart"></i>
                        </a>
           
         </div>
        
</nav>




                <div class="banner">
       <div class="layer">
           <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	</div>
	<div class="alert alert-danger alert-dismissible" id="error" style="display:none;">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	</div>
             
                        <div class="container login-container">
                            <div class="row">

                                <div class="col-md-6 login-form-1">
                                    <h3>التسجيل</h3>
                                    <div id="error1"></div>
                                	<form id="register_form" name="form1" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder=" * الاسم الأخير  *"
                                                value=""    required />
                                            <input type="text" class="form-control" name="frist_name"  id="frist_name" placeholder=" * الاسم الأول"
                                                value="" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control"  name="email" id="email" placeholder="* البريد الإلكتروني "
                                                value="" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password"  class="form-control" id="password" name="password" placeholder="* كلمة السر "
                                                value="" required />
                                        </div>
                                        <div class="form-group">
                                            <input  name="confpassword" type="password" id="confirmPassword"   class="form-control" placeholder="* أكد كلمة السر "
                                                value="" required />
                                        </div>
                                        <div class="form-group check">
                                            <div class="checkbox">
                                                <div class="checkbox-container">
                                                    <input id="checkbox2" type="checkbox">
                                                    <label for="checkbox2">
                                                        سأرى
                                                        <a href="license.html">شروط وأحكام المتجر</a>
                                                        قبل شراء أي منتج
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="button"  id="butsave" name="save" class="btnSubmit" value="تسجيل " />
                                        </div>
                                    </form>
                   



                                    <div class="registrationFormAlert" style="color:green;" id="CheckPasswordMatch"></div>
                                </div>

                                <div class="col-md-6 login-form-2">
                                    <h3>
                                        تسجيل الدخول
                                    </h3>
                                    	<form id="login_form" name="form1" method="post">
		
		                     <div class="form-group">
                                            <input type="text" class="form-control" placeholder=" * البريد الإلكتروني "
                                        id="email_log"  name="email"    required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" placeholder="* كلمة السر"
                                               class="form-control" id="password_log" placeholder="Password" name="password" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="button"class="btnSubmit" value="سجل الدخول" name="save"  id="butlogin" required />

                                            </div>
                                        <div class="form-group checkforget">

                                            <a href="forgotpassword.php" class="ForgetPwd">نسيت كلمة السر؟</a>


                                            <div class="checkbox">
                                                <div class="checkbox-container">
                                                    <input id="checkbox1" type="checkbox">
                                                    <label for="checkbox1">تذكرني</label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>




                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              <footer class="page-footer my-5 text-muted text-center text-small">
            <div class="py-4">

                <ul class="list-inline">
                    <li class="list-inline-item"><a href="about.html">من نحن</a></li>
                    <li class="list-inline-item"><a href="license.html">شروط</a></li>
                    <li class="list-inline-item"><a href="dmca.html">DMCA</a></li>
                </ul>
                <ul class="social0media">

                    <li class="list-inline-item"><a href="https://www.facebook.com/LuckyHelmet0/" target="blank_">
                            <i class="fab fa-facebook-square"></i>
                        </a>
                    </li>

                    <li class="list-inline-item"><a href="https://www.instagram.com/lucky_helmet_st/" target="blank_">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>

                    <li class="list-inline-item"><a href="https://discord.gg/hnRvd5qVxp" target="blank_">
                            <i class="fab fa-discord"></i> </a>
                    </li>

                    <li class="list-inline-item"><a href="https://t.me/luckyhelmet" target="blank_">
                            <i class="fab fa-telegram-plane"></i> </a>
                    </li>

                </ul>

            </div>
            <ul class="list-inline payments">
                <li class="list-inline-item">
                    <img src="img/footer/visa_footer.png" alt="visa">
                </li>
                <li class="list-inline-item">
                    <img src="img/footer/vodafonecash.png" alt="vodafonecash">
                </li>
                <li class="list-inline-item">
                    <img src="img/footer/fawry-pay-english-logo-1.png" alt="fawry-pay">
                </li>
                <p class="my-1 mt-3 lucky0helmet">&copy; lucky helmet 2021</p>
            </ul>
        </footer>

    </div>




    <!-- Jquery -->
    <script src="js/jquery.js"></script>
<!-- start ajax loign and rigstser-->
    
    
<script>
$(document).ready(function() {
    $('#butsave').on('click', function() {
		var frist_name = $('#frist_name').val();
        var last_name = $('#last_name').val();
		var email = $('#email').val();
		var password = $('#password').val();
        var confirmPassword=$("#confirmPassword").val();
        var checkbox2= $('#checkbox2').prop('checked');
        
if(checkbox2==true && password==confirmPassword){
			$.ajax({
				url: "save.php",
				type: "POST",
				data: {
					type: 1,
					frist_name: frist_name,
                    last_name:last_name,
					email: email,
					password: password						
				},
				cache: false,
				success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
					if(dataResult.statusCode==200){
						$("#butsave").removeAttr("disabled");
						$('#register_form').find('input:text').val('');
						$("#success").show();
						$('#success').html('Registration successful !'); 
                        						location.href = "index.php";						

					}
					else if(dataResult.statusCode==201){
						$("#error").show();
						$('#error').html('Email ID already exists !');
					}
					
				}
			});
		}
		else{
          if(checkbox2!=true)   {
              $("#error1").html("يجب الموافقة على الشروط والاحكام");
          }else{
                            $("#error1").html("error password");

          }
 
  	}

	});
	$('#butlogin').on('click', function() {
		var email = $('#email_log').val();
		var password = $('#password_log').val();
		if(email!="" && password!="" ){
			$.ajax({
				url: "save.php",
				type: "POST",
				data: {
					type:2,
					email: email,
					password: password						
				},
				cache: false,
				success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
					if(dataResult.statusCode==200){
						location.href = "index.php";						
					}
					else if(dataResult.statusCode==201){
						$("#error").show();
						$('#error').html('! هناك خطأ في البريد الإلكتروني أو كلمة السر ');
					}
				}
			});
		}
		else{
			alert('Please fill all the field !');
		}
	});
});
</script>
    <!--end-->
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Swiperjs -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- My own script -->
    <script src="js/script.js"></script>
    <script src="js/cart.js"></script>

</body>

</html>
<?php 
    }

?>