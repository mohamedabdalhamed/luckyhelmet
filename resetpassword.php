<?php 
    ob_start();
        session_start();

    include 'conect.php';

    $stmt=$con->prepare("SELECT value FROM setting where id=1  ");
    $stmt->execute();
    $row=$stmt->fetch();
if($row['value']==1){
    
header("Location:undermaintenance.php");
}else{                
if(isset($_SESSION['email'])){
    $email=$_SESSION['email'];
    $stmt=$con->prepare("SELECT * FROM users where email=? AND status=1 ");
    $stmt->execute(array($email));
    if($stmt->rowCount() >0){
        session_unset();
        session_destroy();
        header('location:index.php');
        exit();    
    }
}
  
    

?>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Lucky Helmet (Store)</title>

    <link rel="shortcut icon" type="image/x-icon" href="img/logo.png">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kelly+Slab&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@300;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Aclonica&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Itim&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

    <link rel="stylesheet" href="css/fontawesome/css/all.min.css">

    <!-- <link href="css/cheatsheet.rtl.css" rel="stylesheet"> -->

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">
    <style>
        .alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}   
    </style>
</head>

<body id="resetpassword" class="simple-page">
    <div class="page-container">
        <div class="not-footer">
            <div class="page-header">
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <div class="container-fluid">
                        <a class="navbar-brand logo" href="index.php">
                            <img src="img/logo.png" alt="Logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarCollapse">
                            <ul class="navbar-nav mb-2 mb-md-0">

                                <li class="nav-item active">
                                    <a class="nav-link" aria-current="page" href="index.php">
                                        <i class="fas fa-home"></i>
                                        الرئيسية
                                    </a>
                                </li>
                                <li>
                                    <a class="nav-link" aria-current="page" href="about.php">
                                        <i class="fas fa-question"></i>
                                        من نحن
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="license.php">
                                        <i class="fas fa-file-signature"></i>
                                        الشروط والأحكام
                                    </a>
                                </li>

                           <?php 
                                 if(!isset($_SESSION['username'])){
                                ?> 
                                <li class="nav-item">
                                    <a class="nav-link" href="login.php">
                                        <i class="fas fa-user"></i>
                                        تسجيل الدخول - تسجيل
                                    </a>
                                </li>
                                <?php 
                                
                                 }
                                else{
                                    ?>
                                      <li class="btn-group nav-item">
                                    <button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown"
                                        aria-expanded="false">
                                        <i class="fas fa-user"></i>
 <?php
                                    echo $_SESSION['username'];
                                    ?>                                       </button>
                                    <ul class="dropdown-menu">

                                        <li><a class="dropdown-item" href="boughtproducts.php">
                                                <i class="fas fa-shopping-cart"></i>
                                                طلبات تم شراؤها
                                            </a></li>
                                            
                                        <li><a class="dropdown-item" href="wishlist.php">
                                                <i class="fas fa-heart"></i>
                                                المفضلة
                                            </a></li>

                                      
                                        <!-- <li>
                                            <a class="dropdown-item" href="waitingproducts.php">
                                                <i class="fas fa-cash-register"></i>
                                                طلبات بانظار الدفع
                                            </a>
                                        </li> -->
                                        <li>
                                            <hr class="dropdown-divider">
                                        </li>
                                        <li><a class="dropdown-item" href="usersettings.php">
                                                <i class="fas fa-user-cog"></i>
                                                إعدادات المستخدم
                                            </a></li>

                                        <li><a class="dropdown-item" href="logout.php">
                                                <i class="fas fa-sign-out-alt"></i>
                                                تسجيل الخروج
                                            </a></li>

                                    </ul>
                                </li>
                                    </a>
                                </li>
                                <?php
                                    
                                }
                                ?> 
                            </ul>
                        </div>
                        <a href="checkout.php" class="cart iconRound">

                            <span class="badge bg-secondary rounded-pill">	
</span>

                            <i class="fas fa-shopping-cart"></i>
                        </a>
           
         </div>
        
</nav>

            </div>
            <div class="page-body">
                <div class="col-6 m-auto">
                <form action='<?php echo $_SERVER["PHP_SELF"]?>' method="POST" >
                <div class="form-group">
                        <input type="email" class="form-control"  name="rest" placeholder="* البريد الإلكتروني " required />
                    </div>
                    <div class="form-group">
                        <input type="submit" class=" button" value="تأكيد" />
                    </div>
                    </form>
                </div>
                   
<?php
$rest=$_POST['rest'];
$subject="Rest Password";
  $stmt=$con->prepare("SELECT * FROM users where email=?");
$stmt->execute(array($rest));
$row=$stmt->fetch();

if($stmt->rowCount()>0){



$message=" من اجل تغير الباسورد القديم  https://twotwix.com/luckyhelmet/newpassword.php?rest=".$row['id'];
mail($rest,$subject,$message);
echo '<div class="alert alert-success"> اذهب الي ايميلك</div>';

header( "refresh:5;url=index.php");


}else{

}
?>
            </div>
        </div>
       
        <footer class="page-footer my-5 text-muted text-center text-small">
            <div class="py-4">

                <ul class="list-inline">
                    <li class="list-inline-item"><a href="about.html">من نحن</a></li>
                    <li class="list-inline-item"><a href="license.html">شروط</a></li>
                    <li class="list-inline-item"><a href="dmca.html">DMCA</a></li>
                </ul>
                <ul class="social0media">

                    <li class="list-inline-item"><a href="https://www.facebook.com/LuckyHelmet0/" target="blank_">
                            <i class="fab fa-facebook-square"></i>
                        </a>
                    </li>

                    <li class="list-inline-item"><a href="https://www.instagram.com/lucky_helmet_st/" target="blank_">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>

                    <li class="list-inline-item"><a href="https://discord.gg/hnRvd5qVxp" target="blank_">
                            <i class="fab fa-discord"></i> </a>
                    </li>

                    <li class="list-inline-item"><a href="https://t.me/luckyhelmet" target="blank_">
                            <i class="fab fa-telegram-plane"></i> </a>
                    </li>

                </ul>

            </div>
            <ul class="list-inline payments">
                <li class="list-inline-item">
                    <img src="img/footer/visa_footer.png" alt="visa">
                </li>
                <li class="list-inline-item">
                    <img src="img/footer/vodafonecash.png" alt="vodafonecash">
                </li>
                <li class="list-inline-item">
                    <img src="img/footer/fawry-pay-english-logo-1.png" alt="fawry-pay">
                </li>
                <p class="my-1 mt-3 lucky0helmet">&copy; lucky helmet 2021</p>
            </ul>
        </footer>

    </div>
    <!-- Jquery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Swiperjs -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- My own script -->
    <script src="js/script.js"></script>
    <script src="js/cart.js"></script>

</body>

</html>
<?php
}
?>  