<?php

//fetch_cart.php

session_start();

$total_price = 0;
$total_item = 0;
$count=1;
$output = '';
if(!empty($_SESSION["shopping_carts"]))
{
    foreach($_SESSION["shopping_carts"] as $keys => $values)
    {
        $output .= '
        <li class="list-group-item d-flex justify-content-between lh-sm row">
                                  
             
             
        <div class="card card-min col-9">
        <div class="card-body">
            <div class="offer">
                <div class="card-title">
                    <img src="Admin/upload/av/'.$values['image'].'" alt=""
                        class="card-img-right">
                    <h5>
                    '.$values["product_name"].'
                                             </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3">
        <a href="#0" class="delete-button delete" id="'. $values["product_id"].'">
            <i class="fas fa-times"></i>
        </a>
        <span class="price text-muted">'.$values["product_price"].'EGP</span>
    </div>
    
    <!-- START COANTITY -->
    
    <div class="col-12 my-4 mx-auto">
     
            <div class="qty">
            <div class="btn-group add-minus-btn" role="group"
                                            aria-label="Basic outlined example">
                
            <span class="btn button minus'.$count.'">-</span>
            <input type="hidden"  name="product_id" id="product_ids'.$count.'" value="'.$values['product_id'].'">

            <input type="number" class="n s'.$count.' product-count count" name="qty"  value="'.$values['product_quantity'].'" readonly >
            <span class="btn button plus'.$count.'">+</span>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-12 my-4 mx-auto">
                                        <div class="btn-group add-minus-btn" role="group"
                                            aria-label="Basic outlined example">
                                            <button type="button" class="btn button">-</button>
                                            <input type="number" class="product-count" name="" id="">
                                            <button type="button" class="btn button">+</button>
                                        </div>
                                    </div> -->

                            </li>


        ';
        
      
  
        $total_price = $total_price + ($values["product_quantity"] * $values["product_price"]);
        $total_item = $total_item + 1;
        $count++;
    }

}

else
{
    $output .= '
    <tr>
        <td colspan="5" align="center">
            Your Cart is Empty!
        </td>
    </tr>
    ';
}
$output .= '</table></div>';
$data = array(
    'cart_details'      =>  $output,
    'total_price'       =>   number_format($total_price, 2),
    'total_item'        =>  $total_item
);  

echo json_encode($data);


?>